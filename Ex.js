#!/usr/bin/env node

function factorielRecursif(n) {
    if (n === 0) {
        return 1
    } else {
        let m = Math.floor(n / 2)
        let odd = 1
        for (let i = 1; i <= n; i += 2) {
            odd *= i
        }
        return odd * (1 << m) * factorielRecursif(m)
    }
}

